<?php

namespace App\Controllers;

use App\Models\Movie;
use App\Views\MoviesView;
use App\Views\SingleMovieView;
use App\Views\MovieFormView;

class MoviesController
{
    public function index()
    {
        $movies = Movie::all("title");

        $view = new MoviesView(compact('movies'));
        $view->render();
    }

    public function show()
    {
        $movie = new Movie((int)$_GET['id']);

        $view = new SingleMovieView(compact('movie'));
        $view->render();
    }

    public function create()
    {
        $this->auth->isAdmin();

        $movie = $this->getMovieFormData();

        $view = new MovieFormView(compact('movie'));
        $view->render();
    }

    public function store()
    {

        $movie = new Movie($_POST);

        if (! $movie->isValid()) {
            $_SESSION['movie.form'] = $movie;

            header("Location: ./?page=movie.create");
            exit();
        }

        $movie->save();

        header("Location: ./?page=movie&id=" . $movie->id);
    }

    public function edit()
    {
        $movie = $this->getMovieFormData($_GET['id']);

        $view = new MovieFormView(compact('movie'));
        $view->render();
    }

    public function update()
    {
        $movie = new Movie($_POST['id']);
        $movie->processArray($_POST);

        if (! $movie->isValid()) {
            $_SESSION['movie.form'] = $movie;

            header("Location: ./?page=movie.edit&id=" . $_POST['id']);
            exit();
        }

        $movie->save();

        header("Location: ./?page=movie&id=" . $movie->id);

    }

    public function destroy()
    {
        Movie::destroy($_POST['id']);

        header("Location: ./?page=movies");
    }

    private function getMovieFormData($id = null)
    {
        if (isset($_SESSION['movie.form'])) {
            $movie = $_SESSION['movie.form'];
            unset($_SESSION['movie.form']);
        } else {
            $movie = new Movie($id);
        }
        return $movie;
    }
}
