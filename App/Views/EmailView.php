<?php

namespace App\Views;

use Mailgun\Mailgun;

abstract class EmailView extends View
{

    public function sendEmail($templateFile)
    {
        extract($this->data);
        
        # Instantiate the client.
        $mgClient = new Mailgun('key-cb0f1197675b35ff4072bfc05c30c16b');
        $domain = "sandbox172a26fc0d984109ac8acec77fe0c680.mailgun.org";

        ob_start();

        include $templateFile;

        $emailBody = ob_get_clean();
        
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'    => $emailHeaders['from'],
            'to'      => $emailHeaders['to'],
            'subject' => $emailHeaders['subject'],
            'text'    => $emailBody
        ));
    }
}
