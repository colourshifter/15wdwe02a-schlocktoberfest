      <div class="row">
        <div class="col-xs-12">
          <ol class="breadcrumb">
            <li><a href="./">Home</a></li>
            <li><a href="./?page=movies">Movies</a></li>
            <li class="active"><?= $movie->title ?></li>
          </ol>

          <h1><?= $movie->title ?></h1>
          <p>Released in <?= $movie->year ?></p>
          <p><?= $movie->description ?></p>
          
          <p>
            <a href="./?page=movie.edit&amp;id=<?= $movie->id ?>" class="btn btn-default">
              <span class="glyphicon glyphicon-pencil"></span> Edit Movie
            </a>
          </p>

        </div>
      </div>

